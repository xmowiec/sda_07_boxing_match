package sda.fight;

import sda.fight.repository.AttackType;
import sda.fight.repository.DefendType;
import sda.fight.repository.IActionType;

import java.util.Random;

public class BoxingMatch implements IFightingMatch {

    private IFighter fighter1;
    private IFighter fighter2;
    private IFighter winner;
    private int roundsCount;

    BoxingMatch(IFighter fighter1, IFighter fighter2) {
        this.fighter1 = fighter1;
        this.fighter2 = fighter2;
        this.winner = null;
        this.roundsCount = 0;
    }

    @Override
    public void fight() {
        Random order = new Random();
        // do rounds of match, while winner is unknown
        do {
            if (order.nextBoolean()) {
                winner = round(fighter1, fighter2);
            } else {
                winner = round(fighter2, fighter1);
            }

/*
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
*/
        } while (winner == null);

    }

    /**
     * Handle single round (ane action)
     * @param attacker fighter who call attack
     * @param defender fighter who try to defend
     * @return winner, if defender is not alive
     */
    private IFighter round(IFighter attacker, IFighter defender) {
        roundsCount++;
        AttackType attackType = attacker.attack();
        DefendType defendType = defender.defend();
        boolean roundResult = fighterWasHit(attackType, defendType);

        int attackForce = attacker.getAttackForce();
        if (roundResult) {
            defender.decreaseHp(attackForce);
        }
        printRoundAction(attacker, defender, attackType, defendType, roundResult, attackForce);

        return !defender.isAlive() ? attacker : null;
    }

    private void printRoundAction(IFighter attacker, IFighter defender, AttackType attackType, DefendType defendType, boolean roundResult, int attackForce) {
        System.out.println(roundsCount + ". " +
                attacker.getName() +
                " (" + attacker.getHp() + ")" +
                " zadaje " + attackType.toString() +
                " cios o sile " + attackForce +
                " do " + defender.getName() +
                "" + " (" + defender.getHp() + ")," +
                " który " + (roundResult ? "nieskutecznie" : "skutecznie") + " " +
                "blokuje " + defendType.toString() + ".");
    }

    /**
     * Compare attack type with defend type
     * @param attackType attacker's action
     * @param defendType defender's action
     * @return true, if attacker hits defender
     */
    private boolean fighterWasHit(AttackType attackType, DefendType defendType) {
        boolean hitSuccessful = true;
        if (attackType.equals(AttackType.HIGH_HIT) && defendType.equals(DefendType.HIGH_BLOCK)) {
            hitSuccessful = false;
        }
        if (attackType.equals(AttackType.LOW_HIT) && defendType.equals(DefendType.LOW_BLOCK)) {
            hitSuccessful = false;
        }
        return hitSuccessful;
    }

    @Override
    public void printMatchResult() {
        System.out.println("===========================");
        System.out.println("====   WYNIKI WALKI   =====");
        System.out.println("===========================");
        System.out.println("Walkę wygrywa " + winner.getName() + " wynikiem " + winner.getHp());
        System.out.println("==== STATYSTYKI CIOSÓW =====");
        printFighterStatics(fighter1);
        printFighterStatics(fighter2);
    }

    private static void printFighterStatics(IFighter fighter) {
        System.out.println("=== " + fighter.getName() + " ===");
        System.out.println("==  ciosy  ==");
        // todo merge two loops into one - try to use common interface IActionType
        for (IActionType actionType : fighter.getActionTypeCount().keySet()){
            if (actionType instanceof AttackType){
                System.out.println(actionType + ": " + fighter.getActionTypeCount().get(actionType));
            }
        }

        System.out.println("== blokady ==");
        for (IActionType actionType : fighter.getActionTypeCount().keySet()){
            if (actionType instanceof DefendType){
                System.out.println(actionType + ": " + fighter.getActionTypeCount().get(actionType));
            }
        }
    }
}