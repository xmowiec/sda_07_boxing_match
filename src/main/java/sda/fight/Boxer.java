package sda.fight;

import sda.fight.repository.AttackType;
import sda.fight.repository.DefendType;
import sda.fight.repository.IActionType;
import sda.fight.repository.IFighterStyle;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Boxer implements IFighter {

    private static final int BOUND_PERCENTAGE = 100;
    private String name; // fighter's name
    private IFighterStyle style; // fighting style
    private int hp; // health
    private int baseHp;
    private int attackForce;

    private Map<AttackType, Integer> attackTypeCount = new HashMap<>();
    private Map<DefendType, Integer> defendTypeCount = new HashMap<>();
    private Map<IActionType, Integer> actionTypeCount = new HashMap<>();


    Boxer(String name, IFighterStyle style, int hp, int attackForce) {
        this.name = name;
        this.style = style;
        this.hp = hp;
        this.baseHp = hp;
        this.attackForce = attackForce;
    }

    @Override
    public boolean isAlive() {
        return hp > 0;
    }

    @Override
    public AttackType attack() {
        Random random = new Random();
        int hitProbability = random.nextInt(BOUND_PERCENTAGE);
        AttackType attack = hitProbability < style.getLowPercentage() ?
                AttackType.LOW_HIT : AttackType.HIGH_HIT;
        logAction(attack);
        return attack;
    }

    @Override
    public DefendType defend() {
        Random random = new Random();
        int defendProbability = random.nextInt(BOUND_PERCENTAGE);
        DefendType defend = defendProbability < style.getLowPercentage() ?
                DefendType.LOW_BLOCK : DefendType.HIGH_BLOCK;
        logAction(defend);
        return defend;
    }


    /**
     * Add set into proper map of counters.
     * First action put new key into map,
     * next increment the value.
     *
     * @param action any action type (attack or defend)
     */
    private void logAction(IActionType action) {

        if (!actionTypeCount.containsKey(action)) {
            actionTypeCount.put(action, 1);
        } else {
            Integer counter = actionTypeCount.get(action);
            actionTypeCount.put(action, ++counter);
        }
    }

    @Override
    public void decreaseHp(int attackForce) {
        hp -= attackForce;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHp() {
        return hp;
    }

    @Override
    public int getAttackForce() {
        Random random = new Random();
        // todo correct decrease attack force with hp
        return random.nextInt(attackForce) * hp / baseHp + 1;
    }

    @Override
    public Map<IActionType, Integer> getActionTypeCount() {
        return actionTypeCount;
    }
}
