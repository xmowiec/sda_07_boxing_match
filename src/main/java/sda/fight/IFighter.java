package sda.fight;

import sda.fight.repository.AttackType;
import sda.fight.repository.DefendType;
import sda.fight.repository.IActionType;

import java.util.Map;

public interface IFighter {

    boolean isAlive();

    AttackType attack();

    DefendType defend();

    void decreaseHp(int attackForce);

    String getName();

    int getHp();

    int getAttackForce();

    Map<IActionType, Integer> getActionTypeCount();

}
