package sda.fight;

import sda.fight.repository.IFighterStyle;

public class App {
    public static void main(String[] args) {

        IFighter fighter1 = new Boxer("Mike Tyson", IFighterStyle.HIGH, 100, 8);
        IFighter fighter2 = new Boxer("Andrzej Gołota", IFighterStyle.LOW, 100, 7);
        IFightingMatch match = new BoxingMatch(fighter1, fighter2);
        match.fight();
        match.printMatchResult();
    }
}
