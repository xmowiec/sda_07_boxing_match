package sda.fight.repository;

public enum DefendType implements IActionType {
    HIGH_BLOCK, LOW_BLOCK;

    @Override
    public String toString() {
        if (this.equals(HIGH_BLOCK)){
            return "wysoko";
        }
        if (this.equals(LOW_BLOCK)){
            return "nisko";
        }
        return super.toString();
    }
}
