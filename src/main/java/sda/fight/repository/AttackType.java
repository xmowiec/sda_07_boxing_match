package sda.fight.repository;

public enum AttackType implements IActionType {
    HIGH_HIT, LOW_HIT;

    @Override
    public String toString() {
        if (this.equals(HIGH_HIT)){
            return "wysoki";
        }
        if (this.equals(LOW_HIT)){
            return "niski ";
        }
        return super.toString();
    }
}
