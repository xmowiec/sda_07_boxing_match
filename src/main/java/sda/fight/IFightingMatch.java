package sda.fight;

public interface IFightingMatch {

    /**
     * Handle single fight between fighters of match
     */
    void fight();

    /**
     * Print result of match (the winner name)
     * and statistics of both fighters
     */
    void printMatchResult();
}
